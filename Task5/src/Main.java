import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Yoxlamaq istediyiniz sozu yazin:");
        Scanner input = new Scanner(System.in);
        String word = input.nextLine();

        if (isPalindrome(word)) {
            System.out.println("Soz palindromdur");
        } else {
            System.out.println("Soz palindrom deyil!");
        }
    }

    public static boolean isPalindrome(String word) {
        int length = word.length();

        for (int i = 0; i < length / 2; i++) {
            if (word.charAt(i) != word.charAt(length - i - 1)) {
                return false;
            }
        }

        return true;
    }
}