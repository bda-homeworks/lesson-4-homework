import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Take three numbers from the user
        System.out.print("Enter the first number: ");
        int num1 = scanner.nextInt();
        System.out.print("Enter the second number: ");
        int num2 = scanner.nextInt();
        System.out.print("Enter the third number: ");
        int num3 = scanner.nextInt();

        // Calculate and print the result of using math power to the third number
        for (int i = num1; i <= num2; i++) {
            int result = (int) Math.pow(i, num3);
            System.out.println(i + "^" + num3 + " = " + result);
        }
    }
}