import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Enter the number:");
        Scanner input = new Scanner(System.in);
        int number = input.nextInt();
        numberbreak(number);
    }
    public static void numberbreak (int num) {
        int digit;
        while (num > 0) {
            digit = num % 10;
            System.out.print(digit + " ");
            num /= 10;
        }
    }
}