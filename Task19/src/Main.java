import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Reqeminizi qeyd edin:");
        Scanner input = new Scanner(System.in);
        int num = input.nextInt();
        mathpower(num);
    }
    public static void mathpower (int number) {
        double square = number * number;
        double cube = number * number * number;
        double fourthPower = Math.pow(number, 4);

        System.out.println("Square: " + square);
        System.out.println("Cube: " + cube);
        System.out.println("Fourth power: " + fourthPower);
    }
}