import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        looptekreqemler();
    }
    public static void looptekreqemler () {
        System.out.println("Reqeminizi qeyd edin:");
        Scanner input = new Scanner(System.in);
        int num = input.nextInt();
        System.out.println("Qeyd etdiyiniz reqeme " + num + " qeder olan tek reqemler:");
        for (int i = 1; i <= num; i += 2) {
            System.out.println(i);
        }
    }
}