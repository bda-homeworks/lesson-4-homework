import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Bir reqem qeyd edin:");
        Scanner input = new Scanner(System.in);
        int number = input.nextInt();
        int count = 0;
        while (number != 0) {
            number /= 10;
            ++count;
        }
        System.out.println("Numbers of digits: " + count);
    }
}