import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        char c = input.next().charAt(0);
        boolean AltAlta = input.nextBoolean();
        method(a, c, AltAlta);
    }
    public static void method(int a, char c, boolean AltAlta) {
        while (a >= 1) {
            if (AltAlta) {
                System.out.println(c);
                --a;
            } else {
                System.out.print(c);
                --a;
            }
        }
    }
}