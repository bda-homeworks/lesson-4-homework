import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Necenci reqeme kimi vurma cedvelinin olmagini isteyirsizse hemen reqemi qeyd edin:");
        Scanner input = new Scanner(System.in);
        int num = input.nextInt();

        for (int i = 1; i <= num; i++) {
            for (int j = 1; j <= 10; j++) {
                System.out.printf("%d x %d = %d%n", i, j, i*j);
            }
            System.out.println(); // add a blank line after each row
        }
    }
}