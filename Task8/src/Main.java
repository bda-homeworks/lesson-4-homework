import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Faktorialini bilmek istediyiniz bir reqemi qeyd edin!");
        int i,fact = 1;
        Scanner input = new Scanner(System.in);
        int number = input.nextInt();

        for (i = 1; i <= number; i++) {
            fact = fact * i;
        }
        System.out.println(number + " reqemin faktoriali " + fact + "-dir ");
    }
}