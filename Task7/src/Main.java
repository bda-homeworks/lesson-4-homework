import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String str = input.toString();
        int vCount = 0,cCount = 0;
        str = str.toLowerCase();

        for (int i = 0;i < str.length(); i++) {
            if (str.charAt(i) == 'a'|| str.charAt(i) == 'i' || str.charAt(i) == 'e'|| str.charAt(i) == 'u' ||str.charAt(i) == 'o') {
                vCount++;
            } else if (str.charAt(i) >= 'a' && str.charAt(i) <= 'z') {
                cCount++;
            }
        }
        System.out.println("Number of vowels: " + vCount);
        System.out.println("Number of consonants: " + cCount);
    }
}