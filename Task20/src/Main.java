import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Birinci reqemi qeyd edin:");
        int num1 = input.nextInt();
        System.out.println("Ikinci reqemi qeyd edin:");
        int num2 = input.nextInt();
        multikal(num1, num2);
    }
    public static void multikal (int number1, int number2) {
        int toplama = number1 + number2;
        System.out.println("The sum of the two integer is: " + toplama);
        int cixma = number1 - number2;
        System.out.println("The difference of the two integer is: " + cixma);
        int vurma = number1 * number2;
        System.out.println("The product of the two integer is: " + vurma);
        double avarage = (float) (number1 + number2) / 2;
        System.out.println("The avarage of the two integer is: " + + avarage);
        int dist = Math.abs(number1 - number2);
        System.out.println("The distance of the two integer is: " + dist);
        int min = Math.min(number1, number2);
        int max = Math.max(number1, number2);
        System.out.println("Max integer is: " + max);
        System.out.println("Min integer is: " + min);
    }
}