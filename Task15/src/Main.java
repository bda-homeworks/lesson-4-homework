import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Enter Fahrenheit to convert it Celsius:");
        Scanner input = new Scanner(System.in);
        double number = input.nextInt();
        fahrentocelsi(number);
    }
    public static void fahrentocelsi (double number) {
        Scanner input = new Scanner(System.in);
        double celsi = (number - 32) * 5 / 9;
        System.out.println(number + " degree Fahrenheit is equal to " + celsi + " in Celsius");
    }
}