import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Birinci reqemi qeyd edin:");
        int num1 = input.nextInt();
        System.out.println("Ikinci reqemi qeyd edin:");
        int num2 = input.nextInt();
        MultiCalculator(num1, num2);
    }
    public static void MultiCalculator (int num1, int num2) {
        int toplama = num1 + num2;
        System.out.println(" " + num1 + " + " + num2 + " " + " = " + toplama);
        int cixma = num1 - num2;
        System.out.println(" " + num1 + " - " + num2 + " " + " = " + cixma);
        int vurma = num1 * num2;
        System.out.println(" " + num1 + " * " + num2 + " " + " = " + vurma);
        int bolme = num1 / num2;
        System.out.println(" " + num1 + " / " + num2 + " " + " = " + bolme);
        int qaliq = num1 & num2;
        System.out.println(" " + num1 + " % " + num2 + " " + " = " + qaliq);
    }
}