import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the quantity of numbers to add: ");
        int quantity = scanner.nextInt();

        int sum = 0;

        for (int i = 1; i <= quantity; i++) {
            System.out.print("Enter number " + i + ": ");
            int num = scanner.nextInt();
            sum += num;
        }

        System.out.println("The sum of the numbers is: " + sum);
    }
}