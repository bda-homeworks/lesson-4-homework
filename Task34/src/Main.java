import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Reqemi qeyd edin:");
        int number = input.nextInt();
        ikiyebolunen(number);
    }
    public static void ikiyebolunen(int num) {
        int i = 1;
        while (i <= num) {
            if (i % 2 == 0) {
                System.out.print(i + " ");
            }
            i++;
        }
        /*
        System.out.print("\nUsing for loop: ");
        for (int j = 1; j <= num; j++) {
            if (j / 2 * 2 == j) {
                System.out.print(j + " ");
         */
    }
}