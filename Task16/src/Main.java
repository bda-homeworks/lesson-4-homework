import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Meter-e cevirmek istediyiniz inch-i yazin.");
        inchtometer();
    }
    public static void inchtometer () {
        Scanner input = new Scanner(System.in);
        double number = input.nextDouble();
        double meters = number * 0.0254;
        System.out.println(number + " inch is " + meters + " meters ");
    }
}